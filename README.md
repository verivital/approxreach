approxReach is a prototype software tool to compute an overapproximation of the set of reachable states for continuous systems modeled as ordinary differential equations (ODEs) that is within a pre-specified error bound from the actual set of reachable states, measured according to the Hausdorff metric.

