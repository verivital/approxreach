function [L,M,Mtild] = maximumDynamics(ode,xmin,xmax,ymin,ymax,tmin,tmax)
%MAXIMUMDYNAMICS Calculates the maximum dynamics for the Jacobian and the
%Derivative
xgr=[xmin:0.1:xmax];
ygr=[ymin:0.1:ymax];
tn=[tmin:0.1:tmax];
syms t x_1 x_2 f(t,x_1,y_1) Df(t,x_1,x_2) ft(t,x_1,x_2)
f(t,x_1,y_1)=ode(t,[x_1,y_1]);
func=f(t,x_1,x_2);
Df(t,x_1,x_2)=[diff(func(1),x_1) diff(func(1),x_2);diff(func(2),x_1) diff(func(2),x_2)];
ft(t,x_1,x_2)=diff(f(t,x_1,y_1),t);
max=0;
max2=0;
max3=0;
fd=matlabFunction(f(t,x_1,x_2),'Vars',[t,x_1,x_2]);
Dfd=matlabFunction(Df(t,x_1,x_2),'Vars',[t,x_1,x_2]);
ftd=matlabFunction(ft(t,x_1,x_2),'Vars',[t,x_1,x_2]);
for i=1:length(xgr)
    for j=1:length(ygr)
        for k=1:length(tn)
            xi=xgr(i);
            yi=ygr(j);
            ti=tn(k);
            vec=fd(ti,xi,yi);
            vec2=ftd(ti,xi,yi);
            A=Dfd(ti,xi,yi);
            norm1=norm(A);
            norm2=norm(vec);
            norm3=norm(vec2);
            if(norm1>max2)
                max2=norm1;
            end
            if(norm2>max)
                max=norm2;
            end
            if(norm3>max3)
                max2=norm3;
            end
        end
   end
end
L=double(max2);
Mtild=double(max);
M=(max3+max2*max);
end

