function [xi,hxi,a,b] = genSquarePoints(numpts)
a=-1;
b=1;
griddiscretization=0.01;
pts =ceil(sqrt(numpts + 1));
step = (b - a) / pts;
[X,Y] = ndgrid(a : step : b, a : step : b);
[X1,Y1]=ndgrid(a : griddiscretization : b, a : griddiscretization : b);
xi = zeros(2,numpts);
for i = 1 : pts + 1
    idxs = [(i-1)*pts + i : i*pts + i];
    xi(:, idxs) = [X(i,:); Y(i,:)];
end
%setting up points for Algorithm
initPoints={};
k=1;
for gh=1:length(X)
    for gj=1:length(Y)
            p1=X(gj,gh);
            p2=Y(gj,gh);
            initPoints{k}=[p1,p2];
            %plot(p1,p2,'o');
            %hold on;
            k=k+1;
    end
end
gridPoints={};
k=1;
for gh=1:length(X1)
    for gj=1:length(Y1)
            p1=X1(gj,gh);
            p2=Y1(gj,gh);
            gridPoints{k}=[p1,p2];
            %plot(p1,p2,'o');
            %hold on;
            k=k+1;
    end
end
    
%Calculation of hxi for Example 1 using Algorithim 1
MaxDistance=0;
for jj=1:length(initPoints)
    p1=initPoints{jj};
    MinDistance=(p1(1)-gridPoints{1}(1))^2+(p1(2)-gridPoints{1}(2))^2;
    for ii=2:length(gridPoints)
        p2=gridPoints{ii};
        distance=(p1(1)-p2(1))^2+(p1(2)-p2(2))^2;
        if (MinDistance>distance)
            MinDistance=distance;
        end
    end
    if(MinDistance>MaxDistance)
        MaxDistance=MinDistance;
    end
end
hxi=MaxDistance+(sqrt(2)*step);
exNum=1;
end

