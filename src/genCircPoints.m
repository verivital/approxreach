function [xi,hxi,a,b]=genCircPoints(numpts)
R=1;
xc0=0;
yc0=-2;
griddis=0.01;
%fourpoints
t4=[0,pi/2,pi, 3*pi/2];
xf = xc0 + R*cos(t4);
yf = yc0 + R*sin(t4);
%plot(xf,yf,'dk');
%hold on;
numpts =ceil(sqrt(numpts + 1));
xstep = (xf(1) - xf(3)) / numpts;
ystep=(yf(2) - yf(4))/ numpts;
a=xf(3);
b=xf(1);
[X,Y] = ndgrid(xf(3) : xstep : xf(1), yf(4) : ystep : yf(2));
[X1,Y1] = ndgrid(xf(3) : griddis : xf(1), yf(4) : griddis : yf(2));
initPoints={};

%Setting up pts for hxi calculation
k=1;
for gh=1:length(X)
    for gj=1:length(Y)
            p1=X(gj,gh);
            p2=Y(gj,gh);
            fun=p1^2+(p2+2)^2;
            if((fun<=(R^2)))
                initPoints{k}=[p1,p2];
                xi(:,k)=[p1,p2];
                %plot(p1,p2,'d');
                %hold on;
                k=k+1;
            end
    end
end
gridPoints={};
k=1;
for gh1=1:length(X1)
    for gj1=1:length(Y1)
            p1=X1(gj1,gh1);
            p2=Y1(gj1,gh1);
            fun=p1^2+(p2+2)^2;
            if(fun<=(R^2))
                gridPoints{k}=[p1,p2];
                %plot(p1,p2,'.k');
                %hold on;
                k=k+1;
            end
    end
end
%Calculation of hxi for Example 2 using Algorithim 1
MaxDistance=0;
for jj=1:length(initPoints)
    p1=initPoints{jj};
    MinDistance=(p1(1)-gridPoints{1}(1))^2+(p1(2)-gridPoints{1}(2))^2;
    for ii=2:length(gridPoints)
        p2=gridPoints{ii};
        distance=(p1(1)-p2(1))^2+(p1(2)-p2(2))^2;
        if (MinDistance>distance)
            MinDistance=distance;
        end
    end
    if(MinDistance>MaxDistance)
        MaxDistance=MinDistance;
    end
end
hxi=MaxDistance+(sqrt(2)*xstep);
exNum=2;
end