function [N,Numpts] = calculateNewSteps(desiredBloat,L,M,Tf,a,b)
%CALCULATENEWSTEPS Summary of this function goes here
%   Detailed explanation goes here
halfd=desiredBloat/2;
dn=2*M+((M/2*L)*(exp(L*Tf)-1));
%h=((2*L)/M)*halfd*(1/(exp(L*Tf)-1));
h=2*halfd/dn;
spacing=(1/sqrt(2))*(1/exp(L*Tf))*halfd;
Numpts=(ceil((b-a)/spacing))^2;
N=ceil((1/h));
end

