function xd = ode_example_02(t, x)
	xd(1) = 2*x(1) - x(1)*x(2);
	xd(2) = (2*x(1)^2) - x(2);
end
