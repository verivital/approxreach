function xd = ode_example_lorenz(t, x)
    % https://en.wikipedia.org/wiki/Lorenz_system
    sigma = 10;
    rho = 28;
    beta = 8/3;
	xd(1) = sigma * (x(2) - x(1));
	xd(2) = x(1) * (rho - x(3)) - x(2);
	xd(3) = x(1) * x(2) - beta * x(3);
end
