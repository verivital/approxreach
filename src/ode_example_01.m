function xd = ode_example_01(t, x)
	xd(1) = 1/(1+x(2).^2);
	xd(2) = 1/(1+x(1).^2);
end
