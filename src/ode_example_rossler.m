function xd = ode_example_rossler(t, x)
    % https://en.wikipedia.org/wiki/R%C3%B6ssler_attractor
    a = 0.2;
    b = 0.2;
    c = 5.7;
    % a = 0.1;
    % b = 0.1;
    % c = 14;
	xd(1) = -x(2) - x(3);
	xd(2) = x(1) + a*x(2);
	xd(3) = b + x(3)*(x(1) - c);
end
