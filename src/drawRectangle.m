function drawRectangle(x1,y1,x2,y2,epsilon,color)
	%DRAWRECTANGLE Summary of this function goes here
	%   Detailed explanation goes here
	opt_debug = 0;
	opt_draw_fill = 1;
	opt_draw_faces = 0;
	opt_draw_rectangle = 1;

	point1=[x1,y1];
	point2=[x2,y2];

	originaldirection=[point2(1)-point1(1); point2(2)-point1(2)];
	scale=norm(originaldirection);
	originaldirection=(1/scale).*originaldirection;
	ML=[ 0 1; -1 0];
	MR=[ 0 -1; 1 0];
	directionleft=MR*originaldirection;
	directionright=ML*originaldirection;
	sanitycheck=norm(originaldirection);

	[x1l,y1l]=vectorLine(epsilon,point1,directionleft);
	[x1r,y1r]=vectorLine(epsilon,point1,directionright);
	[x2l,y2l]=vectorLine(epsilon,point2,directionleft);
	[x2r,y2r]=vectorLine(epsilon,point2,directionright);
	
	if opt_debug
		plot(x1l,y1l,'*');
		plot(x1r,y1r,'*');
		plot(x2l,y2l,'*');
		plot(x2r,y2r,'*');
	end
	
	if opt_draw_faces
		plot([x1l x2l], [y1l y2l],strcat('--',color));
		plot([x1r x2r], [y1r y2r],strcat('--',color));
		plot([x1l x1r], [y1l y1r],strcat('--',color));
		plot([x2l x2r], [y2l y2r],strcat('--',color));
	end

	if opt_draw_fill
		fillx=[x1l;x1r;x2r;x2l];
		filly=[y1l;y1r;y2r;y2l];
		fill(fillx,filly,color,'EdgeColor','none');
	end

	if opt_draw_rectangle
		d = epsilon*2;
		px = x1-epsilon;
		py = y1-epsilon;
		rectangle('Position',[px py d d],'Curvature',[1,1],'FaceColor', color,'Edgecolor','none');

		px = x2-epsilon;
		py = y2-epsilon;
		rectangle('Position',[px py d d],'Curvature',[1,1],'FaceColor', color,'Edgecolor','none');
	end
end

