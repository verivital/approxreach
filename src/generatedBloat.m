function bt = generatedBloat(hxi,L,M,T,h)
%GENERATEDBLOAT Summary of this function goes here
%   Detailed explanation goes here
bt=(exp(L*T)*hxi);
bt1=(2*(M)*h);
bt3=(M*h)/(2*L)*(exp(L*T)-1);
bt=bt+bt1+bt3;
end

% %calculates the bloatCalc for Example 1 and Example 2
% function bloat=bloatCalc(hxi,L,Mtild,T,h)
% bloat=exp(L*T)*hxi+(2*(Mtild*L)*h)+(((Mtild*h)/(2*(Mtild*L)))*(exp(L*T)-1));
% end