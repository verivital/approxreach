% This file is the main entry point to the approximate reachability. It: 
% (1) generates a grid of initial conditions, 
% (2) generates trajectories up to a time bound through Euler simulation, 
% (3) computes the error bound that the overapproximation is from the actual set of reachable states under the Hausdorff metric,
% (4) plots the trajectories starting from the initial conditions
% (5) plots the reachable state overapproximation from the trajectories and the computed Hausdorff metric error bound distance between the computed overapproximative reachable states and the analytical (true and in general non-computable) reachable states
%
% Assumptions:
% This is a proof-of-concept numerical prototype at this stage.
% As such, several strong assumptions are made.
% (1) Only 2-d systems are currently supported
%
% Example calls:
% (0) general call:
% - This is a general example call and it will produce the matrix and
% dynamic bounds if they are unknown
%   > approxReach(@ode_example_01, 0, 1, 100, 16,@genSquarePoints)
% 
% (1) ADHS 2018 paper
% - To execute example 1 from the paper, run:
%   > approxReach(@ode_example_01, 0, 1, 100, 16,@genSquarePoints,1,2)

% - To execute example 2 from the paper, run:
%   > approxReach(@ode_example_02, 0, 1, 100, 16,@genCircPoints,65.2,21.5)

% - To view raw trajectories change opt_plot_reach to 0.

% (3)- To choose a desired inflation value for example 1 and example 2
% alter the last parameter and run: (ONLY Run this on a new example if you
% know the bounds for the jacobian and ODE dynamics. Otherwise run the code
% above before running your example as below)
%   > approxReach(@ode_example_01, 0, 1, @genSquarePoints,1,2,1.0)
%   > approxReach(@ode_example_02, 0, 1, @genCircPoints,65.2,21.5,1.0)

% Input arguments:
% - ode_dynamics: function pointer to ODE dynamics
% - time_start: initial time
% - time_final: final time bound
% - N: number of time points to simulate along a trajectory
% - numpts: number of initial states
% -initPts: Initial pts and hxi calculation (genSquarePts generates a
% square grid of inital set and genCircPoints generates pots from a circle
% at a particular point and with a particular radius
% -DYM: Maximum bound for ODE dynamics if known ahead of time. If not
% leave blank
% -DFM: Maximum bound for Jacobian Dynamics if known ahead of time. If not
% leave blank
% -desiredBloat: maximum desired inflation of simulation traces in creating
% reachable set over-approximation. (Only use this parameter if you know the bounds for the matrix norm and 
% the ODE dynamics. Otherwise leave this parameter blank and run the simulations as above to determine these bounds.
% Additionally leave this blank if you want to specify the number of steps
% used in the euler approximation and the number of points sampled from the initial set.)
function approxReach(ode_dynamics, time_start, time_final, N, numpts,initPts,DYM,DFM,desiredBloat)
%Argument handling
    narginchk(4,8)
    if (nargin == 6 && exist('N','var'))
        DYM=0;
        DFM=0;
        opt_specify_bloat=0;
    elseif(nargin==8)
         opt_specify_bloat=0;
    elseif (nargin==7)
        opt_specify_bloat=1;
        DFM=initPts;
        desiredBloat=DYM;
        initPts=N;
        DYM=numpts;
        clear N
        L=DFM;
        Mtild=DYM;
    end
    close all
	
    
    % options
	opt_plot_initial = 1;
	opt_plot_trajectories = 1;
	opt_plot_reach = 1;
	opt_monte_carlo = 0;
    %optionhandler
    once=0;
	% system dimensionality, defined by number of outputs of function
	% handle to system dynamics
	n = nargout(ode_dynamics);
    
    % error handling for input arguments
    if time_start > time_final
        'ERROR: time_final must be greater than time_start'
    end
    
    if numpts <= 0
        'ERROR: numpts must be a positive integer (natural number)'
    end
    
    if( opt_specify_bloat)
        if(DYM>0)
            [xi,hxi,a,b] = genSquarePoints(1);
            clear xi hxi
            [N,numpts] = calculateNewSteps(desiredBloat,DFM,(DFM*DYM),time_final,a,b);
        end
        if(N>10^7 && numpts>10^7)
            disp('The desired bloat value would take too long to compute in the current version of this prototype')
            message2=sprintf('The number of points sampled from the initial set would be: %s.',num2str(round(numpts,0)));
            message3=sprintf('The number of steps in the euler simulation would be: %s',num2str(round(N,0)));
            disp(message2); 
            disp(message3); 
            return
        end
        
    end
    
    
    % initial state generation
    [xi,hxi] = initPts(numpts);
	time_duration = time_final - time_start;
	hin2=(time_duration)/N; % actual time step
	%specify the bloat calculating it
    %bloat=errorCalc(hxi,hin2,exNum);
	% set up time vector for h_in2 Euler time step
	tvec_hin2 = [time_start:hin2:time_final];

	figure;
	hold on;

	% simulate from initial states
	for i2=1:length(xi)
		% set initial states at initial time
		xvec=[];
        xvec(1,:) = xi(:,i2);

		if opt_plot_initial
            plot3(xi(1,i2), xi(2,i2), time_start, 'o'); % initial state
		end

		% Euler simulation of trajectory with N time points (uniform)
		for k=1:N
            xeval = ode_dynamics(tvec_hin2(k), xvec(k,:));
            xvec(k+1,:)=xvec(k,:) + hin2*xeval;
		end

		if opt_plot_trajectories
			plot3(xvec(:,1), xvec(:,2) ,tvec_hin2, '-.');
        end
        
        
        
        if opt_plot_reach
            if(DFM==0 && DYM==0)
                if(once<1)
                    ax=gca;
                    xset=ax.XLim;
                    yset=ax.YLim;
                    xa=min(xset(1),yset(1));
                    xb=max(xset(2),yset(2));
                    if(abs(xb)>abs(xa))
                        xa=-xb;
                    elseif(abs(xa)>abs(xb))
                        xb=-xa;
                    end
                    [L,M,Mtild] = maximumDynamics(ode_dynamics,xa,xb,xa,xb,time_start,time_final);
                    bloat = generatedBloat(hxi,L,M,time_final,hin2);
                end
            else 
                if (once<1)
                    bloat=generatedBloat(hxi,DFM,DFM*DYM,time_final,hin2)
                end
            end
            for bi=1:(length(xvec(:,1)) - 1)
				drawRectangle(xvec(bi,1),xvec(bi,2),xvec(bi+1,1),xvec(bi+1,2),bloat,'b');
            end
            if(once<1)
                if(DFM==0 && DYM==0)
                    message=sprintf('The Jacobian is Bounded by: %s.',num2str(round(L,3)));
                    message1=sprintf('The ODE dynamics are bounded by: %s',num2str(round(Mtild,3)));
                    disp(message); 
                    disp(message1); 
                end
            end
            once=once+1;
        end
    end
    
    

	title('Forward Euler approximation','Interpreter','latex' )
	xlabel('$x$','Interpreter','latex');
	ylabel('$y$','Interpreter','latex');
	zlabel('time (s)');
end
